# Use Ubuntu 20.04 as the base image.
#FROM ubuntu:20.04 AS base
FROM python:3.10-slim-bullseye
ENV TZ=Europe/Berlin

##### developer / build stage ##################################################

# Install essential build tools and utilities.
RUN apt update -y && apt upgrade -y
RUN apt install -y --no-install-recommends \
    git \
    tzdata \
    python3 \
    python3-pip \
    re2c \
    rsync \
    ssh-client \
    nano
    
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone  

RUN git clone --depth 1 --recursive https://gitlab.desy.de/fs-ec-demonstrators/ophyd-tango-demo.git /opt/tango-demo

RUN apt install -y curl
RUN curl -L -O "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh"
RUN bash Miniforge3-$(uname)-$(uname -m).sh -b

WORKDIR /opt/tango-demo
RUN /root/miniforge3/bin/mamba install --file requirements.txt -c conda-forge
WORKDIR /opt/tango-demo/ophyd-async
RUN python3 -m pip install -e .

